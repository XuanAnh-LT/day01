DROP DATABASE IF EXISTS QLSV;

CREATE DATABASE QLSV;

USE QLSV;

CREATE TABLE DMKHOA(
    MaKH Varchar(6),
    TenKhoa Varchar(10),
    PRIMARY KEY(MaKH)
);

CREATE TABLE SINHVIEN(
    MaSV Varchar(6),
    HoSV Varchar(30),
    TenSV Varchar(15),
    GioiTinh Char(1),
    NgaySinh DateTime,
    NoiSinh Varchar(50),
    DiaChi Varchar(50),
    MaKH Varchar(6),
    HocBong Int,
    PRIMARY KEY(MaSV)
);